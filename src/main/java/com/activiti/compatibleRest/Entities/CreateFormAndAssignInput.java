package com.activiti.compatibleRest.Entities;

import java.util.List;

public class CreateFormAndAssignInput {

    private List<FormFields> formFieldsList;
    private List<OutcomesParameters> outcomesParametersList;

    public List<FormFields> getFormFieldsList() {
        return formFieldsList;
    }

    public CreateFormAndAssignInput() {
    }

    public CreateFormAndAssignInput(List<FormFields> formFieldsList, List<OutcomesParameters> outcomesParametersList) {
        this.formFieldsList = formFieldsList;
        this.outcomesParametersList = outcomesParametersList;
    }

    public void setFormFieldsList(List<FormFields> formFieldsList) {
        this.formFieldsList = formFieldsList;
    }

    public List<OutcomesParameters> getOutcomesParametersList() {
        return outcomesParametersList;
    }

    public void setOutcomesParametersList(List<OutcomesParameters> outcomesParametersList) {
        this.outcomesParametersList = outcomesParametersList;
    }
}
