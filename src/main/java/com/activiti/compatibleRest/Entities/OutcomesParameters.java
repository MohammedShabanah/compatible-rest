package com.activiti.compatibleRest.Entities;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class OutcomesParameters implements Serializable{

@JsonProperty("id")
private Integer id ;
@JsonProperty("name")
private String name ;


    public OutcomesParameters() {
    }

    public OutcomesParameters(Integer id  , String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
