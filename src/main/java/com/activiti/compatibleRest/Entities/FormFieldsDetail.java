package com.activiti.compatibleRest.Entities;


import java.util.List;

public class FormFieldsDetail {

private String fieldType = "FormField";
private String id;
private String name ;
private String type ;
private String value ;
private Boolean required ;
private Boolean readOnly;

private Boolean overrideId;
private String placeholder;
private String layout ;
private int sizeX =0;
private int sizeY =0;
private Params params ;


    public FormFieldsDetail(String fieldType, String id, String name, String type, String value, Boolean required, Boolean readOnly, Boolean overrideId, String placeholder, String layout, int sizeX, int sizeY, Params params) {
        this.fieldType = fieldType;
        this.id = id;
        this.name = name;
        this.type = type;
        this.value = value;
        this.required = required;
        this.readOnly = readOnly;
        this.overrideId = overrideId;
        this.placeholder = placeholder;
        this.layout = layout;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getOverrideId() {
        return overrideId;
    }

    public void setOverrideId(Boolean overrideId) {
        this.overrideId = overrideId;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public  String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }
}
