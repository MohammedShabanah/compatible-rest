package com.activiti.compatibleRest.Entities;

import io.swagger.annotations.ApiModelProperty;

public class LoginResquest {

    @ApiModelProperty(example = "admin")
    private String username ;
    @ApiModelProperty(example = "admin")
    private String password ;


    public LoginResquest() {
    }

    public LoginResquest(String username, String password) {
        this.username = username;
        this.password = password;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
