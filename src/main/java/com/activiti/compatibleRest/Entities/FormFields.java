package com.activiti.compatibleRest.Entities;


import io.swagger.annotations.ApiModelProperty;

public class FormFields {

    @ApiModelProperty(example = "firstname")
    private String name ;
    @ApiModelProperty(example = "text")
    private String type ;

    public FormFields() {
    }

    public FormFields(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
