package com.activiti.compatibleRest.Entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "ACT_RU_VARIABLE")
public class RuntimeVariable {
    @Id
    @Column(name="ID_",nullable = false)
    private String id ;

    @Column(name = "REV_")
    private int rev ;

    @Column(name = "TYPE_", nullable = false)
    private String type ;

    @Column(name = "NAME_", nullable = false)
    private String name ;

    @Column(name = "EXECUTION_ID_")
    private String executionId;

    @Column(name = "PROC_INST_ID_")
    private String processInstanceId ;

    @Column(name = "TASK_ID_")
    private String taskId ;

    @Column(name = "BYTEARRAY_ID_")
    private String byteArrayId;

    @Column(name = "DOUBLE_")
    private String Adouble;

    @Column(name = "LONG_")
    private String Along;

    @Column(name = "TEXT_")
    private String text;

    @Column(name = "TEXT2_")
    private String text2;



    public RuntimeVariable() {
    }

    public RuntimeVariable(String id, int rev, String type, String name, String executionId, String processInstanceId, String taskId, String byteArrayId, String adouble, String along, String text, String text2) {
        this.id = id;
        this.rev = rev;
        this.type = type;
        this.name = name;
        this.executionId = executionId;
        this.processInstanceId = processInstanceId;
        this.taskId = taskId;
        this.byteArrayId = byteArrayId;
        Adouble = adouble;
        Along = along;
        this.text = text;
        this.text2 = text2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRev() {
        return rev;
    }

    public void setRev(int rev) {
        this.rev = rev;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getByteArrayId() {
        return byteArrayId;
    }

    public void setByteArrayId(String byteArrayId) {
        this.byteArrayId = byteArrayId;
    }

    public String getAdouble() {
        return Adouble;
    }

    public void setAdouble(String adouble) {
        Adouble = adouble;
    }

    public String getAlong() {
        return Along;
    }

    public void setAlong(String along) {
        Along = along;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    @Override
    public String toString() {
        return "RuntimeVariable{" +
                "id='" + id + '\'' +
                ", rev=" + rev +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", executionId='" + executionId + '\'' +
                ", processInstanceId='" + processInstanceId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", byteArrayId='" + byteArrayId + '\'' +
                ", Adouble=" + Adouble +
                ", Along=" + Along +
                ", text='" + text + '\'' +
                ", text2='" + text2 + '\'' +
                '}';
    }
}
