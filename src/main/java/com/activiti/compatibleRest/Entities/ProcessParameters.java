package com.activiti.compatibleRest.Entities;


import java.io.Serializable;
import java.util.List;

public class ProcessParameters implements Serializable {

private String name ;
private String key ;
private String description ;
private List<FormFields> formFields;

    public ProcessParameters() {
    }

    public ProcessParameters(String name, String key, String description, List<FormFields> formFields) {
        this.name = name;
        this.key = key;
        this.description = description;
        this.formFields = formFields;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FormFields> getFormFields() {
        return formFields;
    }



    public void setFormFields(List<FormFields> formFields) {
        this.formFields = formFields;
    }
    @Override
    public String toString() {
        return "ProcessParameters{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", description='" + description + '\'' +
                ", formFields=" + formFields +
                '}';
    }

}
