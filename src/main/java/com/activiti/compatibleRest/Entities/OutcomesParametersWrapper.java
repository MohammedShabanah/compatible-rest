package com.activiti.compatibleRest.Entities;

import java.util.List;

public class OutcomesParametersWrapper {

    private List<OutcomesParameters> outcomesParameters;

    public List<OutcomesParameters> getOutcomesParameters() {
        return outcomesParameters;
    }

    public void setOutcomesParameters(List<OutcomesParameters> outcomesParameters) {
        this.outcomesParameters = outcomesParameters;
    }
}
