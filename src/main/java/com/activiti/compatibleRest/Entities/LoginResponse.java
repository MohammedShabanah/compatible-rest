package com.activiti.compatibleRest.Entities;

public class LoginResponse {

    private String Cookie ;
    private String Authorization ;

    public LoginResponse() {
    }

    public LoginResponse(String cookie, String authorization) {
        Cookie = cookie;
        Authorization = authorization;
    }

    public String getCookie() {
        return Cookie;
    }

    public void setCookie(String cookie) {
        Cookie = cookie;
    }

    public String getAuthorization() {
        return Authorization;
    }

    public void setAuthorization(String authorization) {
        Authorization = authorization;
    }
}
