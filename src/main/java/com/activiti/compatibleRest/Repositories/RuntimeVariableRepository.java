package com.activiti.compatibleRest.Repositories;

import com.activiti.compatibleRest.Entities.Form;
import com.activiti.compatibleRest.Entities.RuntimeVariable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RuntimeVariableRepository extends JpaRepository<RuntimeVariable, Long> {

    @Query("SELECT R FROM RuntimeVariable R WHERE R.name =:name AND R.text =:text")
    List<RuntimeVariable> findByNameAndText(@Param(value = "name") String name ,@Param(value = "text") String text);
}