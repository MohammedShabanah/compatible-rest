package com.activiti.compatibleRest.Repositories;

import com.activiti.compatibleRest.Entities.Form;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FormRepository extends JpaRepository<Form, String> {

    List<Form> findByName(String name);
//    List<Form> findById(String id);
}