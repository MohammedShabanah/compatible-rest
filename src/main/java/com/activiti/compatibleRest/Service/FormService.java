package com.activiti.compatibleRest.Service;

import com.activiti.compatibleRest.Entities.Form;
import com.activiti.compatibleRest.Repositories.FormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormService {

    @Autowired
    FormRepository formRepository;


    public List<Form> getFormByName(String name){

        return formRepository.findByName(name);
    }

    public Form getFormById(String id){

        return formRepository.findOne(id);
    }
}
