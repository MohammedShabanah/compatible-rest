package com.activiti.compatibleRest.Service;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.activiti.compatibleRest.Entities.*;
import com.activiti.compatibleRest.Repositories.RuntimeVariableRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class ProcessService {

    @Autowired
    LoginService loginService;

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    RuntimeVariableRepository runtimeVariableRepository;

    @Value("${activiti.host}")
    private String activitiHost;

    @Value("${activiti.port}")
    private String activitiPort;


    RestTemplate restTemplate;


    public ResponseEntity<String> createProcess(String Cookie, String Authorization, ProcessParameters processParameters) {
        final String uri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/models";
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);

        JsonObject jsonObj = new JsonObject();

        jsonObj.addProperty("name", processParameters.getName());
        jsonObj.addProperty("key", processParameters.getName());
        jsonObj.addProperty("description", processParameters.getName());
        jsonObj.addProperty("modelType", 0);

        HttpEntity<String> entity = new HttpEntity<String>(jsonObj.toString(), headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.println(result);
        return result;
    }


    public ResponseEntity<String> createForm(String Cookie, String Authorization, String formName, List<FormFields> formFields) {
        final String uri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/models";
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);
//        {\"name\": " +  formName + ", \n" +
//                "\"key\": \"" +  formName+ "_key" + "\",\n" +
//                "\"description\":  " +  formName + ", \n" +
//                "\"modelType\": 2}
        JsonObject jsonObj = new JsonObject();

        jsonObj.addProperty("name", formName);
        jsonObj.addProperty("key", formName);
        jsonObj.addProperty("description", formName);
        jsonObj.addProperty("modelType", 1);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObj.toString(), headers);
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.println(result);
        return result;
    }

    public ResponseEntity<String> createFormJust(String Cookie, String Authorization, String formName) {
        final String uri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/models";
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);
//        {\"name\": " +  formName + ", \n" +
//                "\"key\": \"" +  formName+ "_key" + "\",\n" +
//                "\"description\":  " +  formName + ", \n" +
//                "\"modelType\": 2}
        JsonObject jsonObj = new JsonObject();

        jsonObj.addProperty("name", formName);
        jsonObj.addProperty("key", formName);
        jsonObj.addProperty("description", formName);
        jsonObj.addProperty("modelType", 2);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObj.toString(), headers);
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.println(result);
        return result;
    }

    public void assignFormToProcess(String Cookie, String Authorization, String formId, String processId) {

        //   authenticationService.getAuthentication(cookie);
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);

        final String Uri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/models/";
        HttpEntity<String> Entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> proccessResult = restTemplate.exchange(Uri + processId, HttpMethod.GET, Entity, String.class);
        ResponseEntity<String> formResult = restTemplate.exchange(Uri + formId, HttpMethod.GET, Entity, String.class);

        JsonParser jsonParser = new JsonParser();
        JsonObject oForm = jsonParser.parse(formResult.getBody()).getAsJsonObject();
        JsonObject oProcess = jsonParser.parse(proccessResult.getBody()).getAsJsonObject();

        System.out.println("oProcess :" + oProcess);

        HttpHeaders assignHeaders = new HttpHeaders();
        assignHeaders.add("Authorization", Authorization);
        assignHeaders.add("Cookie", Cookie);
        assignHeaders.add("Accept", "application/json");
        //   assignHeaders.add("Content-Type" , "application/x-www-form-urlencoded");
        assignHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);


        String json_xml = "{\"resourceId\":" + oProcess.getAsJsonObject().getAsJsonPrimitive("id") + "," +
                "\"properties\":{" +
                "\"process_id\":" + oProcess.getAsJsonObject().getAsJsonPrimitive("id") + "," +
                "\"name\":\"" + oProcess.getAsJsonObject().getAsJsonPrimitive("name").getAsString() + "\"," +
                "\"documentation\":\"el7ob\"," +
                "\"process_author\":\"" + oProcess.getAsJsonObject().getAsJsonPrimitive("createdBy").getAsString() + "\"," +
                "\"process_version\":\"\"," +
                "\"process_namespace\":\"http://www.activiti.org/processdef\"," +
                "\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\"," +
                "\"messagedefinitions\":\"\"}," +
                "\"stencil\":{\"id\":\"BPMNDiagram\"}," +
                "\"childShapes\":[{\"resourceId\":\"startEvent1\"," +
                "\"properties\":{\"overrideid\":\"\",\"name\":\"\"," +
                "\"documentation\":\"\",\"executionlisteners\":\"\"," +
                "\"initiator\":\"\",\"formkeydefinition\":\"\"," +
                "\"formreference\":{" +
                "\"id\":" + oForm.getAsJsonObject().getAsJsonPrimitive("id") +
                ",\"name\":" + oForm.getAsJsonObject().getAsJsonPrimitive("name") +
                ",\"key\":" + oForm.getAsJsonObject().getAsJsonPrimitive("key") +
                "}," +
                "\"formproperties\":\"\"}," +
                "\"stencil\":{\"id\":\"StartNoneEvent\"}," +
                "\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193}," +
                "\"upperLeft\":{\"x\":100,\"y\":163}}," +
                "\"dockers\":[]}]," +
                "\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050}," +
                "\"upperLeft\":{\"x\":0,\"y\":0}},\"stencilset\":" +
                "{\"url\":\"stencilsets/bpmn2.0/bpmn2.0.json\",\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"}," +
                "\"ssextensions\":[]}";

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("modeltype", "model");
        map.add("json_xml", json_xml);
        map.add("name", oProcess.getAsJsonPrimitive("name").toString());
        map.add("key", oProcess.getAsJsonPrimitive("key").toString());
        map.add("description", oProcess.getAsJsonPrimitive("description").toString());
        map.add("newversion", "true");
        map.add("comment", "");
        map.add("lastUpdated", oProcess.getAsJsonObject().getAsJsonPrimitive("lastUpdated").getAsString());
        map.add("conflictResloveAction", "newVersion");


        System.out.println("json :" + json_xml);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, assignHeaders);
        System.out.println("request :" + request);

        ResponseEntity<String> result = restTemplate.exchange(Uri + processId + "/editor/json", HttpMethod.POST, request, String.class);

        System.out.println("result:" + result);

    }

    public void assignFormToUserTask(String Cookie, String Authorization, String formId, String processId, String userTaskId) {

        //   authenticationService.getAuthentication(cookie);
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);

        final String Uri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/models/";
        HttpEntity<String> Entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> proccessResult = restTemplate.exchange(Uri + processId, HttpMethod.GET, Entity, String.class);
        ResponseEntity<String> formResult = restTemplate.exchange(Uri + formId, HttpMethod.GET, Entity, String.class);

        JsonParser jsonParser = new JsonParser();
        JsonObject oForm = jsonParser.parse(formResult.getBody()).getAsJsonObject();
        JsonObject oProcess = jsonParser.parse(proccessResult.getBody()).getAsJsonObject();

        System.out.println("oProcess :" + oProcess);

        HttpHeaders assignHeaders = new HttpHeaders();
        assignHeaders.add("Authorization", Authorization);
        assignHeaders.add("Cookie", Cookie);
        assignHeaders.add("Accept", "application/json");
        //   assignHeaders.add("Content-Type" , "application/x-www-form-urlencoded");
        assignHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);


        String json_xml = "{\"resourceId\":" + oProcess.getAsJsonObject().getAsJsonPrimitive("id") + "," +
                "\"properties\":{" +
                "\"process_id\":" + oProcess.getAsJsonObject().getAsJsonPrimitive("id") + "," +
                "\"name\":" + oProcess.getAsJsonObject().getAsJsonPrimitive("name").getAsString() + "," +
                "\"documentation\":\"el7ob\"," +
                "\"process_author\":" + "\"" + oProcess.getAsJsonObject().getAsJsonPrimitive("createdBy").getAsString() + "\"," +
                "\"process_version\":\"\"," +
                "\"process_namespace\":\"http://www.activiti.org/processdef\"," +
                "\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\"," +
                "\"messagedefinitions\":\"\"}," +
                "\"stencil\":{\"id\":\"BPMNDiagram\"}," +
                "\"childShapes\":[{\"resourceId\":\"" + userTaskId + "\"," +
                "\"properties\": {\n" +
                "\"overrideid\": \"\",\n" +
                "\"name\": \"\",\n" +
                "\"documentation\": \"\",\n" +
                "\"asynchronousdefinition\": \"false\",\n" +
                "\"exclusivedefinition\": \"false\",\n" +
                "\"executionlisteners\": \"\",\n" +
                "\"multiinstance_type\": \"None\",\n" +
                "\"multiinstance_cardinality\": \"\",\n" +
                "\"multiinstance_collection\": \"\",\n" +
                "\"multiinstance_variable\": \"\",\n" +
                "\"multiinstance_condition\": \"\",\n" +
                "\"isforcompensation\": \"false\",\n" +
                "\"usertaskassignment\": \"\",\n" +
                "\"formkeydefinition\": \"\"," +
                "\"formreference\":{" +
                "\"id\":" + oForm.getAsJsonObject().getAsJsonPrimitive("id") +
                ",\"name\":" + oForm.getAsJsonObject().getAsJsonPrimitive("name").toString() +
                ",\"key\":" + oForm.getAsJsonObject().getAsJsonPrimitive("key").toString() +
                "}," +
                "        \"duedatedefinition\": \"\",\n" +
                "        \"prioritydefinition\": \"\",\n" +
                "        \"formproperties\": \"\",\n" +
                "        \"tasklisteners\": \"\"" + "}," +
                "\"stencil\":{\"id\":\"UserTask\"}," +
                "\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193}," +
                "\"upperLeft\":{\"x\":100,\"y\":163}}," +
                "\"dockers\":[]}]," +
                "\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050}," +
                "\"upperLeft\":{\"x\":0,\"y\":0}},\"stencilset\":" +
                "{\"url\":\"stencilsets/bpmn2.0/bpmn2.0.json\",\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"}," +
                "\"ssextensions\":[]}";

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("modeltype", "model");
        map.add("json_xml", json_xml);
        map.add("name", oProcess.getAsJsonObject().getAsJsonPrimitive("name").getAsString());
        map.add("key", oProcess.getAsJsonObject().getAsJsonPrimitive("key").getAsString());
        map.add("description", oProcess.getAsJsonObject().getAsJsonPrimitive("description").getAsString());
        map.add("newversion", "true");
        map.add("comment", "");
        map.add("lastUpdated", oProcess.getAsJsonObject().getAsJsonPrimitive("lastUpdated").getAsString());
        map.add("conflictResloveAction", "newVersion");


        System.out.println("json :" + json_xml);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, assignHeaders);
        System.out.println("request :" + request);

        ResponseEntity<String> result = restTemplate.exchange(Uri + processId + "/editor/json", HttpMethod.POST, request, String.class);

        System.out.println("result:" + result);

    }


    public String getFormOutcomes(String Cookie, String Authorization, String formId) {

        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);

        final String formUri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/form-models/";
        HttpEntity<String> formEntity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> formResult = restTemplate.exchange(formUri + formId, HttpMethod.GET, formEntity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {

            Map<String, Object> map = mapper.readValue(formResult.getBody(), new TypeReference<Map<String, Object>>() {
            });
            Map<String, Object> formDefinition = (Map<String, Object>) map.get("formDefinition");

            System.out.println("\"outcomes\" : " + mapper.writeValueAsString(formDefinition.get("outcomes")));

            return mapper.writeValueAsString(formDefinition.get("outcomes"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        return formUri;
    }


    public void setFormfields(String Cookie, String Authorization, String formId, List<FormFields> formFields) {
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);

        final String formUri ="http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/form-models/";


        HttpEntity<String> formEntity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> formResult = restTemplate.exchange(formUri + formId, HttpMethod.GET, formEntity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);


        JsonParser jsonParser = new JsonParser();
        JsonObject o = jsonParser.parse(formResult.getBody()).getAsJsonObject();
        JsonArray jsonArray = o.getAsJsonObject("formDefinition").getAsJsonArray("fields");

        //  System.out.println(">>>>>>>>>>"+jsonArray.toString());

        List<FormFieldsDetail> formFieldsDetails = new ArrayList<>();
        for (FormFields f : formFields) {
            formFieldsDetails.add(new FormFieldsDetail("FormField",
                    f.getName(),
                    f.getName(),
                    f.getType(),
                    null,
                    false,
                    false,
                    false,
                    null,
                    null,
                    0,
                    0,
                    new Params()));
        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = new Gson();
        gson = builder.serializeNulls().create();

        JsonElement element = gson.toJsonTree(formFieldsDetails, new TypeToken<List<FormFieldsDetail>>() {
        }.getType());

        JsonArray formFieldsDetails_jsonArray = element.getAsJsonArray();

        o.getAsJsonObject("formDefinition").add("name", o.get("name"));
        o.getAsJsonObject("formDefinition").add("key", o.get("key"));
        o.getAsJsonObject("formDefinition").add("fields", formFieldsDetails_jsonArray);
        if (o.getAsJsonObject("formDefinition").getAsJsonArray("outcomes") == null) {

            o.getAsJsonObject("formDefinition").add("outcomes", new JsonArray());
        }

        String formDefinitionJson = o.toString().replace("\\", "");
        System.out.println(">>>>>json :" + formDefinitionJson);


        HttpEntity<String> editFormEntity = new HttpEntity<String>("{\"reusable\": \"false\", \n" +
                "\"newVersion\": \"false\",\n" +
                "\"comment\": \" \",\n" +
                "\"formRepresentation\": " +
                formDefinitionJson + ",\"formImageBase64\":" +
                "\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABxCAYAAABvGp7oAAABjklEQVR4nO3UoQ0AMAzAsP7/cdH2wtgUycA8KLO7B6BgfgcAvDIsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIusixtE4mZOd0AAAAASUVORK5CYII=\"}", headers);
        ResponseEntity<String> editFormResult = restTemplate.exchange(formUri + formId, HttpMethod.PUT, editFormEntity, String.class);

        System.out.println(editFormResult);


    }

    public String editFormOutcomes(String Cookie, String Authorization, String formId, List<OutcomesParameters> outcomesParameters) {
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", Authorization);
        headers.add("Content-Type", "application/json");
        headers.add("Cookie", Cookie);

        final String formUri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/rest/form-models/";
        HttpEntity<String> formEntity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> formResult = restTemplate.exchange(formUri + formId, HttpMethod.GET, formEntity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        String JSONObject = null;
        try {

            Map<String, Object> map = mapper.readValue(formResult.getBody(), new TypeReference<Map<String, Object>>() {
            });


            JsonParser jsonParser = new JsonParser();
            JsonObject o = jsonParser.parse(formResult.getBody()).getAsJsonObject();
            JsonArray jsonArray = o.getAsJsonObject("formDefinition").getAsJsonArray("outcomes");

            List<OutcomesParameters> formOutcomes = new ArrayList<>();
            JsonArray outcomes_jsonArray = new JsonArray();
            System.out.println("outcomesParameters :" + outcomesParameters);
            if (!outcomesParameters.isEmpty()) {

                for (OutcomesParameters f : outcomesParameters) {
                    formOutcomes.add(new OutcomesParameters(f.getId(), f.getName()));
                }
                GsonBuilder builder = new GsonBuilder();
                Gson gson = new Gson();
                gson = builder.serializeNulls().create();

                JsonElement element = gson.toJsonTree(formOutcomes, new TypeToken<List<OutcomesParameters>>() {
                }.getType());

                outcomes_jsonArray = element.getAsJsonArray();
                System.out.println("outcomes_jsonArray :" + formOutcomes.size());
            }
            o.getAsJsonObject("formDefinition").add("outcomes", outcomes_jsonArray);
            String formDefinitionJson = o.toString().replace("\\", "");
            System.out.println(">>>>>json :" + formDefinitionJson);
            HttpEntity<String> editFormEntity = new HttpEntity<String>("{\"reusable\": \"false\", \n" +
                    "\"newVersion\": \"false\",\n" +
                    "\"comment\": \" \",\n" +
                    "\"formRepresentation\": " +
                    formDefinitionJson + ",\"formImageBase64\":" +
                    "\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABxCAYAAABvGp7oAAABjklEQVR4nO3UoQ0AMAzAsP7/cdH2wtgUycA8KLO7B6BgfgcAvDIsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIMC8gwLCDDsIAMwwIyDAvIMCwgw7CADMMCMgwLyDAsIMOwgAzDAjIusixtE4mZOd0AAAAASUVORK5CYII=\"}", headers);
            ResponseEntity<String> editFormResult = restTemplate.exchange(formUri + formId, HttpMethod.PUT, editFormEntity, String.class);

            System.out.println(editFormResult);

            return editFormResult.getBody().toString();


        } catch (IOException e) {
            e.printStackTrace();
            return "This Form Not Found";
        }


    }

    public List<RuntimeVariable> findRuntimeVariableByNameAndText(String name , String text){
        return runtimeVariableRepository.findByNameAndText(name,text);
    }

}
