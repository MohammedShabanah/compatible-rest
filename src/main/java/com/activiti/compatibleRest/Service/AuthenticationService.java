package com.activiti.compatibleRest.Service;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthenticationService {
    public static void main(String[] args) {
        //getAuthentication();
    }

    public  void getAuthentication(String cookie)
    {
        final String uri = "http://localhost:8080/activiti-app/app/rest/authenticate";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept" , "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", "Basic " + "YWRtaW46YWRtaW4=");
        headers.add("Cookie" , cookie);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

        System.out.println(result);
    }
}
