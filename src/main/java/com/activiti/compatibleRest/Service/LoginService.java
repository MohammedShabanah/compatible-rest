package com.activiti.compatibleRest.Service;


import com.activiti.compatibleRest.Entities.LoginResponse;
import com.activiti.compatibleRest.Entities.LoginResquest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LoginService {

    @Autowired
    AuthenticationService authenticationService;
    @Value("${activiti.host}")
    private String activitiHost;

    @Value("${activiti.port}")
    private String activitiPort;
    public static void main(String[] args) {
        //activitiLogin();
    }


    public LoginResponse activitiLogin(LoginResquest loginResquest){

      //  authenticationService.getAuthentication();
        final String uri = "http://"+activitiHost+":" +activitiPort+"/activiti-app/app/authentication";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept" , "application/json, text/plain, */* ,application/*+json");
        headers.add("Authorization", "Basic " + "YWRtaW46YWRtaW4=");
        headers.add("Content-Type" , "application/x-www-form-urlencoded; charset=UTF-8");
        headers.add("Cookie" , "ACTIVITI_REMEMBER_ME=UDZnM2JqYXpNVE5CV2Zvd0tuMUlXdz09OnhlZ2txMGhrZVgwMFhBZUFscXdhSWc9PQ");

        HttpEntity<String> entity = new HttpEntity<String>("j_username=" + loginResquest.getUsername() + "&j_password=" +
                loginResquest.getPassword() + "&_spring_security_remember_me=" +
                "true" +
                "&submit=LoginResponse", headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

        String[] cookie = result.getHeaders().get("Set-Cookie").toString().split(";");
        String cookieToken = cookie[0].replace("[" , "");
        System.out.println(cookieToken);

        return new LoginResponse (cookieToken , "Basic YWRtaW46YWRtaW4=");


    }

}
