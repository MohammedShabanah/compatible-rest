package com.activiti.compatibleRest.Config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2

public class SwaggerConfig extends WebMvcConfigurerAdapter{

    @Value("${server.port}")
    private String serverPort;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host("localhost:"+serverPort)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.activiti.compatibleRest.Api"))
                .paths(PathSelectors.ant("/*"))
                .build()
                .apiInfo(apiInfo());


    }


    private ApiInfo apiInfo() {
        return new ApiInfo(
                "My REST ACTIVITI API",
                "Some custom description of API.",
                "API ",
                "Terms of service",
                 new Contact("Ahmed Magdy", "www.activiti.org", "ahmedmagdy.success@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }



}