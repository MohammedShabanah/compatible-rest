package com.activiti.compatibleRest.Api;


import com.activiti.compatibleRest.Entities.*;
import com.activiti.compatibleRest.Service.FormService;
import com.activiti.compatibleRest.Service.ProcessService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/compatible-rest")
@CrossOrigin(origins = "*",allowedHeaders = "*")
public class ActivitiRestController {

    @Autowired
    ProcessService processService;

    @Autowired
    FormService formService;


    @RequestMapping(value = "/testService")
    public ResponseEntity testApi(){
        int x = 5 ;
        return ResponseEntity.ok(true);
    }

    @GetMapping(value = "/getFormByName" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Form> getFormByName(@RequestHeader(name = "Cookie") String Cookie , @RequestHeader(name = "Authorization") String Authorization ,@RequestParam(name = "formName") String formName) {
       return formService.getFormByName(formName);

    }

    @GetMapping(value = "/getOutcomes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getOutcomes(@RequestHeader(name = "Authorization") String Authorization,
                                      @RequestParam(name = "formId") String formId) {
        try {

//            List<Form> formList =  formService.getFormByName(formName);
            Form formList=  formService.getFormById(formId);

            if(formList!=null){
                String Outcomes = processService.getFormOutcomes("ACTIVITI_REMEMBER_ME=Z3VWenFrVmFuRlRhYkRQcS9qQ29xUT09Oll3WTJ0TDc5bTN0ZFdPTWVYZFUzQmc9PQ",
                        Authorization,
                        formList.getId());
                return ResponseEntity.status(HttpStatus.OK).body(Outcomes);
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(null);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();

        }

    }

    @PostMapping(value = "/editOutcomes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity editOutcomes(@RequestHeader(name = "Cookie") String Cookie, @RequestHeader(name = "Authorization") String Authorization, @RequestParam(name = "formId") String formId, @RequestBody OutcomesParametersWrapper outcomesParameters) {
        try {

            String Outcomes = processService.editFormOutcomes(Cookie, Authorization, formId, outcomesParameters.getOutcomesParameters());
            return ResponseEntity.status(HttpStatus.OK).body(Outcomes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();


        }

    }

//    //////////////////////////// Create Form ///////////////////////////////////////////////////////
//    @ApiOperation(value = "createForm" , notes = "Create Form" ,response =ResponseEntity.class , responseContainer = "object")
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "Success"),
//            @ApiResponse(code = 400, message = "Bad Request"),
//            @ApiResponse(code = 401, message = "Unauthorized"),
//            @ApiResponse(code = 403, message = "Method not Allowed"),
//            @ApiResponse(code = 404, message = "Not Found"),
//            @ApiResponse(code = 500, message = "Server Error")})
//    @PostMapping(value = "/createForm" , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity createForm(@RequestHeader(name = "Cookie") String Cookie , @RequestHeader(name = "Authorization") String Authorization ,@RequestParam(name = "formName") String formName,@RequestBody List<FormFields> formFields) {
//        try {
//            ResponseEntity<String> formResult = processService.createFormJust(Cookie, Authorization,formName);
//            ObjectMapper mapper = new ObjectMapper();
//            if(!formFields.isEmpty()){
//                Map<String, Object> formMap = mapper.readValue(formResult.getBody(), new TypeReference<Map<String,Object>>(){});
//                String formId = (String) formMap.get("id");
//                processService.setFormfields(Cookie, Authorization,formId,formFields);
//            }
//            return ResponseEntity.status(HttpStatus.OK).body(formResult.getBody());
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
//        }
//
//    }

    @PostMapping(value = "/createProcess", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createProcess(@RequestHeader(name = "Cookie") String Cookie, @RequestHeader(name = "Authorization") String Authorization, @RequestBody ProcessParameters processParameters) {
        try {

            ResponseEntity<String> proccessResult = processService.createProcess(Cookie, Authorization, processParameters);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            Map<String, Object> proccessMap = mapper.readValue(proccessResult.getBody(), new TypeReference<Map<String, Object>>() {
            });
            String proccessId = (String) proccessMap.get("id");

            ResponseEntity<String> formResult = processService.createFormJust(Cookie, Authorization, processParameters.getName().toString());
            Map<String, Object> formMap = mapper.readValue(formResult.getBody(), new TypeReference<Map<String, Object>>() {
            });
            String formId = (String) formMap.get("id");
            if (!processParameters.getFormFields().isEmpty()) {
                processService.setFormfields(Cookie, Authorization, formId, processParameters.getFormFields());
            }
            processService.assignFormToProcess(Cookie, Authorization, formId, proccessId);

            return ResponseEntity.status(HttpStatus.OK).body("{\"Process\" :" + proccessResult.getBody() + "," + "\"Form\" :" + formResult.getBody() + "}");
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @PostMapping(value = "/assignFormToProcess", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity assignFormToProcess(@RequestHeader(name = "Cookie") String Cookie, @RequestHeader(name = "Authorization") String Authorization, @RequestParam(name = "formId") String formId, @RequestParam(name = "processId") String processId) {
        try {
            processService.assignFormToProcess(Cookie, Authorization, formId, processId);

            return ResponseEntity.status(HttpStatus.OK).body("{ \"message\" : \"Form is successfully assigned\"}");
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @PostMapping(value = "/assignFormToUserTask", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity assignFormToUserTask(@RequestHeader(name = "Cookie") String Cookie, @RequestHeader(name = "Authorization") String Authorization, @RequestParam(name = "formId") String formId, @RequestParam(name = "processId") String processId, @RequestParam(name = "userTaskId") String userTaskId) {
        try {
            processService.assignFormToUserTask(Cookie, Authorization, formId, processId, userTaskId);

            return ResponseEntity.status(HttpStatus.OK).body("{ \"message\" : \"Form is successfully assigned to the user task\"}");
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }
//    /////////////////////////////Create Form and assign Form to User Task //////////////////////////////////////////////////////////////////
//    @ApiOperation(value = "createFormAndAssignUserTask" , notes = "create form and edit outcomes and assign user task" ,response =ResponseEntity.class , responseContainer = "object")
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "Success"),
//            @ApiResponse(code = 400, message = "Bad Request"),
//            @ApiResponse(code = 401, message = "Unauthorized"),
//            @ApiResponse(code = 403, message = "Method not Allowed"),
//            @ApiResponse(code = 404, message = "Not Found"),
//            @ApiResponse(code = 500, message = "Server Error")})
//    @PostMapping(value = "/createFormAndAssignUserTask"  , produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity createFormAndAssignUserTask(@RequestHeader(name = "Cookie") String Cookie , @RequestHeader(name = "Authorization") String Authorization , @RequestParam(name = "processId") String processId , @RequestParam(name = "userTaskId") String userTaskId , @RequestParam(name = "formName") String formName, @RequestBody CreateFormAndAssignInput createFormAndAssignInput) {
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//            ResponseEntity<String> formResult = processService.createForm(Cookie, Authorization, formName,createFormAndAssignInput.getFormFieldsList());
//            Map<String, Object> formMap = mapper.readValue(formResult.getBody(), new TypeReference<Map<String,Object>>(){});
//            String formId = (String) formMap.get("id");
//            processService.setFormfields(Cookie, Authorization,formId,createFormAndAssignInput.getFormFieldsList());
//            String Outcomes = processService.editFormOutcomes(Cookie, Authorization,formId , createFormAndAssignInput.getOutcomesParametersList());
//            processService.assignFormToUserTask(Cookie, Authorization,formId  , processId , userTaskId);
//
//            return ResponseEntity.status(HttpStatus.OK).body("{ \"form\" :" + formResult.getBody()+", \"message\" : \"Form is successfully created and assigned to the user task\"}");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
//
//
//        }
//
//    }

    @PostMapping(value = "/createFormWithFieldsAndOutcomes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createFormWithFieldsAndOutcomes(@RequestHeader(name = "Cookie") String Cookie, @RequestHeader(name = "Authorization") String Authorization, @RequestParam(name = "formName") String formName, @RequestBody CreateFormAndAssignInput createFormAndAssignInput) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            ResponseEntity<String> formResult = processService.createForm(Cookie, Authorization, formName, createFormAndAssignInput.getFormFieldsList());
            if (!createFormAndAssignInput.getFormFieldsList().isEmpty()) {
                Map<String, Object> formMap = mapper.readValue(formResult.getBody(), new TypeReference<Map<String, Object>>() {
                });
                String formId = (String) formMap.get("id");
                processService.setFormfields(Cookie, Authorization, formId, createFormAndAssignInput.getFormFieldsList());
                if (!createFormAndAssignInput.getOutcomesParametersList().isEmpty()) {
                    String Outcomes = processService.editFormOutcomes(Cookie, Authorization, formId, createFormAndAssignInput.getOutcomesParametersList());
                }
            }


            return ResponseEntity.status(HttpStatus.OK).body("{ \"form\" :" + formResult.getBody() + ", \"message\" : \"Form is successfully created and assigned to the user task\"}");
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();


        }

    }

    @GetMapping(value = "/getProcessInstanceId" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String>  getProcessInstanceId(@RequestHeader(name = "Cookie") String Cookie , @RequestHeader(name = "Authorization") String Authorization ,@RequestParam(name = "name") String name ,@RequestParam(name = "value") String value ) {
        List<RuntimeVariable> runtimeVariableList = processService.findRuntimeVariableByNameAndText(name,value);
        List<String> processInstanceIds = runtimeVariableList.stream().map(runtimeVariable -> runtimeVariable.getProcessInstanceId()).collect(Collectors.toList());
        return processInstanceIds;

    }



    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/start-hire-process", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void startHireProcess(@RequestBody Map<String, String> data) {

        Applicant applicant = new Applicant(data.get("name"), data.get("email"), data.get("phoneNumber"));
//        applicantRepository.save(applicant);

        Map<String, Object> vars = Collections.<String, Object>singletonMap("applicant", applicant);
//        runtimeService.startProcessInstanceByKey("hireProcessWithJpa", vars);
    }


}

