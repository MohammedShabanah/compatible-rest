package com.activiti.compatibleRest.Api;


import com.activiti.compatibleRest.Entities.*;
import com.activiti.compatibleRest.Service.LoginService;
import com.activiti.compatibleRest.Service.ProcessService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
//@RequestMapping("/activiti")
public class ActivitiLoginController {

    @Autowired
    LoginService loginService;


/////////////////////////set outcomes //////////////////////////////////////////////////////////////////////
@ApiOperation(value = "login" , notes = "login with username and password" ,response =ResponseEntity.class , responseContainer = "object")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Method not Allowed"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Server Error")})
@PostMapping(value = "/login" , consumes = MediaType.APPLICATION_JSON_VALUE ,produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity login(@RequestBody LoginResquest loginResquest) {
    try {

         LoginResponse loginResponse = loginService.activitiLogin(loginResquest);
         return ResponseEntity.status(HttpStatus.OK).body(loginResponse);
    } catch (Exception ex) {
        ex.printStackTrace();
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
    }

}





}
